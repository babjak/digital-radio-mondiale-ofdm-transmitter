%		DRM 64QAM HMmix UEP encoder for MSC
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "channel encoder"
%egyseg feladatait latja el az MSC csatornan. A beerkezo biteket kodolja, 
%"megkeveri" majd a definialt konstellacios diagramm alapjan komplex erteku
%cellakat szamit.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%data_in:   Az MSC csatorna adatbitjeit tartalmazo vektor.
%
%VSPP_data_in:   Az MSC csatorna erosen vedett adatbitjeit tartalmazo vektor.
%
%protection_level_1:	Az erosen vedett resz (part A) vedelmi szintjet adja meg.
%
%protection_level_2:	A gyengen vedett resz (part B) vedelmi szintjet adja meg.
%
%X:	Az "A" resz byte-jainak szama, ahogy az SDC-ben jelezve van.
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%A bemeno parameterek alapjan a program az MSC csatorna adatait kodolja.
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%


function [data_out] = drm_MSC_64_HMmix_UEP_encoder(data_in, VSPP_data_in, protection_level_1, protection_level_2, X, robustness_mode, spectrum_occupancy)



%***************************************
%bemeno parameterek ellenorzese
data_out = [];

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end

if isstr(protection_level_1) 
	disp('protection_level_1 ertekei 0, 1, 2, 3 lehet');
	return;
end

protection_level_1 = rem(round(abs(protection_level_1)),4);

if isstr(protection_level_2) 
	disp('protection_level_2 ertekei 0, 1, 2, 3 lehet');
	return;
end

protection_level_2 = rem(round(abs(protection_level_2)),4);

if protection_level_2 <=protection_level_1
	disp('A protection_level_2-nek magasabbnak kell lenni a protection_level_1-nel');
	return;
end
    
%********************************************
%valtozok kezdoertekeinek beallitasa
R_table = [1/2 1/4 3/10 1/2 3/5 3/4 20; 4/7 1/3 4/11 2/3 8/11 4/5 165; 3/5 1/2 4/7 3/4 7/8 7/8 56; 2/3 2/3 2/3 4/5 8/9 8/9 45];

RYlcm_1 = R_table(protection_level_1+1,7);
RYlcm_2 = R_table(protection_level_2+1,7);

Rp_1_re = R_table(protection_level_1+1,1:2:5);
Rp_2_re = R_table(protection_level_2+1,1:2:5);
Rp_1_im = R_table(protection_level_1+1,2:2:6);
Rp_2_im = R_table(protection_level_2+1,2:2:6);


RXp_RYp_table = [1/4 3/10 1/3 4/11 1/2 4/7 3/5 2/3 8/11 3/4 4/5 7/8 8/9; 1 3 1 4 1 4 3 2 8 3 4 7 8; 4 10 3 11 2 7 5 3 11 4 5 8 9];

QAM_MSC_cell_number_table = [1259 1422 2632 2959 5464 6118; 966 1110 2051 2337 4249 4774; 0 0 0 1844 0 3867; 0 0 0 1226 0 2606];

NMUX = QAM_MSC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

p_max = 3;

%part A cells
N1 = ceil(8*X/(RYlcm_1*(Rp_1_im(1)+Rp_1_re(2)+Rp_1_re(3)+Rp_1_im(2)+Rp_1_im(3)))) * RYlcm_1;
if N1 > 20
	N1 = 20;
end
%part B cells
N2 = NMUX - N1;

for I=1:p_max
	RXp_1_re(I) = RXp_RYp_table(2, contains(Rp_1_re(I), RXp_RYp_table, 1));
	RYp_1_re(I) = RXp_RYp_table(3, contains(Rp_1_re(I), RXp_RYp_table, 1));
end

for I=1:p_max
	RXp_1_im(I) = RXp_RYp_table(2, contains(Rp_1_im(I), RXp_RYp_table, 1));
	RYp_1_im(I) = RXp_RYp_table(3, contains(Rp_1_im(I), RXp_RYp_table, 1));
end

for I=1:p_max
	RXp_2_re(I) = RXp_RYp_table(2, contains(Rp_2_re(I), RXp_RYp_table, 1));
	RYp_2_re(I) = RXp_RYp_table(3, contains(Rp_2_re(I), RXp_RYp_table, 1));
end

for I=1:p_max
	RXp_2_im(I) = RXp_RYp_table(2, contains(Rp_2_im(I), RXp_RYp_table, 1));
	RYp_2_im(I) = RXp_RYp_table(3, contains(Rp_2_im(I), RXp_RYp_table, 1));
end

%L1=0;
%for I=1:p_max
%	L1 = L1 + 2*N1*Rp(I);
%end

%L2=0;
%for I=1:p_max
%	L2 = L2 + RXp*floor((2*N2-12)/RYp);
%end

M_re(1,1) = 0;
M_im(1,1) = N1*Rp_1_im(1);

M_re(1,2) = RXp_2_re(1)*floor((N1+N2-12)/RYp_2_re(1));
M_im(1,2) = RXp_2_im(1)*floor((N2-12)/RYp_2_im(1));

for I=2:p_max
%jobban vedett bitekre
	M_re(I,1) = N1*Rp_1_re(I);
%kevesbe vedett bitekre
	M_im(I,1) = N1*Rp_1_im(I);
end

for I=2:p_max
%jobban vedett bitekre
	M_re(I,2) = RXp_2_re(I)*floor((N2-12)/RYp_2_re(I));
%kevesbe vedett bitekre
	M_im(I,2) = RXp_2_im(I)*floor((N2-12)/RYp_2_im(I));
end


temp = 0;
temp = temp + M_im(1,1);
temp = temp + M_re(2,1);
temp = temp + M_im(2,1);
temp = temp + M_re(3,1);
temp = temp + M_im(3,1);
temp = temp + M_im(1,2);
temp = temp + M_re(2,2);
temp = temp + M_im(2,2);
temp = temp + M_re(3,2);
temp = temp + M_im(3,2);



if size(data_in, 2) ~= temp | size(data_in, 1) ~= 1
   disp(sprintf('data_in-nak egy sorvektornak kell lennie %d elemmel', temp));
   return;
end

if size(VSPP_data_in, 2) ~= M_re(1,2) | size(VSPP_data_in, 1) ~= 1
   disp(sprintf('VSPP_data_in-nak egy sorvektornak kell lennie %d elemmel', M_re(1,2)));
   return;
end

rp_re(1) = (N1+N2-12)-RYp_2_re(1)*floor((N1+N2-12)/RYp_2_re(1));

for I=2:p_max
	rp_re(I) = (N2 -12)-RYp_2_re(I)*floor((N2-12)/RYp_2_re(I));
end

for I=1:p_max
	rp_im(I) = (N2 -12)-RYp_2_im(I)*floor((N2-12)/RYp_2_im(I));
end

a = 1 / sqrt(42);


%********************************************
%szamitasok
temp = 0;
%particionalas es kodolas
%erosen vedett
%p=0
v0_1_im = encoder(data_in(temp+1:temp+M_im(1,1)), 0, 0, Rp_1_im(1), 0);
temp = temp + M_im(1,1);

%p=1
v1_1_re = encoder(data_in(temp+1:temp+M_re(2,1)), 0, 0, Rp_1_re(2), 0);  
temp = temp + M_re(2,1);

%p=1
v1_1_im = encoder(data_in(temp+1:temp+M_im(2,1)), 0, 0, Rp_1_im(2), 0);  
temp = temp + M_im(2,1);

%p=2
v2_1_re = encoder(data_in(temp+1:temp+M_re(3,1)), 0, 0, Rp_1_re(3), 0); 
temp = temp + M_re(3,1);

%p=2
v2_1_im = encoder(data_in(temp+1:temp+M_im(3,1)), 0, 0, Rp_1_im(3), 0); 
temp = temp + M_im(3,1);

%gyengen vedett
%p=0
v0_2_im = encoder(data_in(temp+1:temp+M_im(1,2)), 0, 1, Rp_2_im(1), rp_im(1));
temp = temp + M_im(1,2);

, rp(2)); 
%p=1
v1_2_re = encoder(data_in(temp+1:temp+M_re(2,2)), 0, 1, Rp_2_re(2), rp_re(2));
temp = temp + M_re(2,2);

%p=1
v1_2_im = encoder(data_in(temp+1:temp+M_im(2,2)), 0, 1, Rp_2_im(2), rp_im(2));
temp = temp + M_im(2,2);


%p=2
v2_2_re = encoder(data_in(temp+1:temp+M_re(3,2)), 0, 1, Rp_2_re(3), rp_re(3));
temp = temp + M_re(3,2);

%p=2
%v2_2_im = encoder(data_in(temp+1:temp+M_im(3,2)), 0, 1, Rp_2_im(3), rp(3));
v2_2_im = encoder(data_in(temp+1:end), 0, 1, Rp_2_im(3), rp_im(3));



%VSPP
v0_2_re =  encoder(VSPP_data_in(1:end), 0, 1, Rp_2_re(1), rp_re(1)); 


%interleaving
%p=0
y0_re = v0_2_re;

y0_im = v0_2_im;

%p=1
y1_re = [interleaving(v1_1_re, 13) interleaving(v1_2_re, 13)];

y1_im = [interleaving(v1_1_im, 13) interleaving(v1_2_im, 13)];

%p=2
y2_re = [interleaving(v2_1_re, 21) interleaving(v2_2_re, 21)];

y2_im = [interleaving(v2_1_im, 21) interleaving(v2_2_im, 21)];


while length(y0)
	temp1 = y0_re(1)*1 + y1_re(1)*2 + y2_re(1)*4;
    temp2 = y0_im(1)*1 + y1_im(1)*2 + y2_im(1)*4;

	data_out(end+1) = (7-2*temp1)*a + (7-2*temp2)*a*j;

   	y0_re = y0_re(2:end);
	y1_re = y1_re(2:end);
	y2_re = y2_re(2:end);
	y0_im = y0_im(2:end);
	y1_im = y1_im(2:end);
	y2_im = y2_im(2:end);
end

return;

%Foprogram vege
%********************************************
%Seged fuggvenyek


function data_out = interleaving(data_in, tp)
	new_values = pi_function(length(data_in), tp) + 1;
	for I = 1:length(data_in)
		data_out(I) = data_in(new_values(I));
	end

	return





  

%		DRM interleaver
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "OFDM cell mapper"
%es "pilot generator" egysegek feladatait latja el. Figyelembe veszi a 
%frekvencia, az ido es az amplitudo referencia cellak-at, ezeket 
%automatikusan a szabvanynak megfeleloen helyezi el a DRM jelben. A program 
%a DRM jel egy "superframe"-jenek cellakiosztasat szamitja ki.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%MSC_data_values:   A DRM jel egy super frame-jenek MSC cellainak 
%           modulacios ertekeit adja meg ez a
%			vektor. Azaz az MSC csatornabol erkezo adatokat tartalmazza.
%			Megjegyzes: A program elso lepeskent ezen ertekek
%			MSC_modulation_type alapu modolusat veszi. Tehat peldaul
%			MSC_modulation_type = 16 es MSC_data_value = 
%			[15 31 65...] akkor a moduluskepzes utan (15 mod 16,
%			31 mod 16, 65 mod 16) az uj MSC_data_value vektor 
%			erteke [15 15 1...] lesz, mig MSC_modulation_type = 64 eseten
%			[15 31 1...].
%
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%A bemeno parameterek alapjan a program a cellakat elhelyezi az ido frekvencia
%racson.
%
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%
%
%	MSC cella szam( MSC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	3777    4266    7896    8877    16392   18354
%	B	2898    3330    6153    7011    12747   14322
%	C	-       -       -       5532    -       11601
%	D	-       -       -       3678    -       7818
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%
%
%



function [data_out] = drm_3_sm_coding(data_in, protection_level, X)



%***************************************
%bemeno parameterek ellenorzese

    
%********************************************
%valtozok kezdoertekeinek beallitasa

R_table = [1/4 1/2 3/4 4; 1/3 2/3 4/5 15; 1/2 3/4 7/8 8; 2/3 4/5 8/9 45];
RYlcm = R_table(protection_level+1,4);
Rp = R_table(protection_level+1,1:3);

RXp_RYp_table = [1/4 3/10 1/3 4/11 1/2 4/7 3/5 2/3 8/11 3/4 4/5 7/8 8/9; 1 3 1 4 1 4 3 2 8 3 4 7 8; 4 10 3 11 2 7 5 3 11 4 5 8 9];
RXp = [2, contains(R0, RXp_RYp_table, 1)];
RYp = [3, contains(R0, RXp_RYp_table, 1)];


p_max = 3;

QAM_MSC_cell_number_table = [1259 1422 2632 2959 5464 6118; 966 1110 2051 2337 4249 4774; 0 0 0 1844 0 3867; 0 0 0 1226 0 2606];

NMUX = QAM_MSC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);



N1 = ceil(8X/(2*RYlcm*(Rp(1)+Rp(2)+Rp(3)))) * RYlcm;
N2 = NMUX - N1;

L1=0;
for I=1:p_max
    L1 = L1 + 2*N1*Rp(I);
end

L2=0;
for I=1:p_max
    L2 = L2 + RXp*floor((2*N2-12)/RYp);
end

for I=1:p_max
    M(I,1) = 2*N1*Rp(I);
    M(I,2) = RXp*floor((2*N2-12)/RYp);
end

%p=0
%L1

encoder(data_in(1:M(1,1))) 
encoder(data_in(M(1,1)+M(2,1)+M(3,1)+1:M(1,1)+M(2,1)+M(3,1)+M(1,2)))
encoder(data_in(M(1,1)+1:M(1,1)+M(2,1))) 
encoder(data_in(M(1,1)+M(2,1)+M(3,1)+M(1,2)+1:M(1,1)+M(2,1)+M(3,1)+M(1,2)+M(2,2)))
encoder(data_in(M(1,1)+M(2,1)+1:M(1,1)+M(2,1)+M(3,1))) 
encoder(data_in(M(1,1)+M(2,1)+M(3,1)+M(1,2)+M(2,2)+1:end))






%********************************************
%szamitasok
%particionalas





return;





function new_values = pi_function(NMUX)
    new_values(1) = 0;
    
    s = 2 ^ ceil(log2(NMUX));
    q = s/4 - 1;
    t0 = 5;

    for I = 2:NMUX;
        new_values(I) = rem((t0*new_values(I-1)+q),s);
        while new_values(I) > NMUX
            new_values(I) = rem((t0*new_values(I)+q),s);
        end;
    end;
    
    return;
    
 function data_out = encoder(data_in)
    length = length(data_in);
    data_in = [0 0 0 0 0 0 data_in 0 0 0 0 0 0]

    I = 7;
    while length+6 >= I
        data_out(I-6) = rem ((data_in(I) + data_in(I-2) + data_in(I-3) + data_in(I-5) + data_in(I-6)), 2);
        I = I + 1;

        data_out(I-6) = rem ((data_in(I) + data_in(I-1) + data_in(I-2) + data_in(I-3) + data_in(I-6)), 2);
        I = I + 1;

        data_out(I-6) = rem ((data_in(I) + data_in(I-1) + data_in(I-4) + data_in(I-6)), 2);
        I = I + 1;

        data_out(I-6) = rem ((data_in(I) + data_in(I-2) + data_in(I-3) + data_in(I-5) + data_in(I-6)), 2);
        I = I + 1;
        
    end
    
    
    
    return;
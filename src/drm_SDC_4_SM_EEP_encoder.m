%		DRM 4QAM SM EEP encoder for SDC
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "channel encoder"
%egyseg feladatait latja el az SDC csatornan. A beerkezo biteket kodolja, 
%"megkeveri" majd a definialt konstellacios diagramm alapjan komplex erteku
%cellakat szamit.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%data_in:   A SDC csatorna adatbitjeit tartalmazo vektor.
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%A bemeno parameterek alapjan a program az MSC csatorna adatait kodolja.
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%


function [data_out] = drm_SDC_4_SM_EEP_encoder(data_in, robustness_mode, spectrum_occupancy)



%***************************************
%bemeno parameterek ellenorzese
data_out = [];

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end


    
%********************************************
%valtozok kezdoertekeinek beallitasa
R_table = [1/2];
Rp = R_table(1:1);

RXp_RYp_table = [1/4 3/10 1/3 4/11 1/2 4/7 3/5 2/3 8/11 3/4 4/5 7/8 8/9; 1 3 1 4 1 4 3 2 8 3 4 7 8; 4 10 3 11 2 7 5 3 11 4 5 8 9];

QAM_SDC_cell_number_table = [167 190 359 405 754 846; 130 150 282 322 588 662; 0 0 0 288 0 607; 0 0 0 152 0 332];

NSDC = QAM_SDC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

p_max = 1;

N1 = 0;
N2 = NSDC;

for I=1:p_max
	RXp(I) = RXp_RYp_table(2, contains(Rp(I), RXp_RYp_table, 1));
	RYp(I) = RXp_RYp_table(3, contains(Rp(I), RXp_RYp_table, 1));
end

%L1=0;
%for I=1:p_max
%	L1 = L1 + 2*N1*Rp(I);
%end

%L2=0;
%for I=1:p_max
%	L2 = L2 + RXp*floor((2*N2-12)/RYp);
%end

for I=1:p_max
	M(I,2) = RXp(I)*floor((2*N2-12)/RYp(I));
end

if size(data_in, 2) ~= ( M(1,2) ) | size(data_in, 1) ~= 1
   disp(sprintf('data_in-nak egy sorvektornak kell lennie %d elemmel',  M(1,2) ));
   return;
end


for I=1:p_max
	rp(I) = (2*N2 -12)-RYp(I)*floor((2*N2-12)/RYp(I));
end

a = 1 / sqrt(2);


%********************************************
%szamitasok

%particionalas es kodolas
%p=0
v0 = encoder(data_in(1:end)), 0, 1, Rp(1), rp(1)); 

%interleaving
%p=0
y0 = interleaving(v0, 21);



while length(y0)
	temp1 = y0(1)*1;
    temp2 = y0(2)*1;

	data_out(end+1) = (1-2*temp1)*a + (1-2*temp2)*a*j;
   	y0 = y0(3:end);
end

return;

%Foprogram vege
%********************************************
%Seged fuggvenyek


function data_out = interleaving(data_in, tp)
	new_values = pi_function(length(data_in), tp) + 1;
	for I = 1:length(data_in)
		data_out(I) = data_in(new_values(I));
	end

	return





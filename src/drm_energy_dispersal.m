%		DRM energy dispersal
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "energy dispersal"
%egyseg feladatait latja el. A kodolatlan adatokat a szabvanyban definialt
%modon modositja, igy az "veletlenszerubb" lesz.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%datat_in:  A csatorna bitjeit tartalmazo sorvektor.
%
%A bemeno parameterek alapjan a program a cellakat elhelyezi az ido frekvencia
%racson.
%
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%
%
%



function [data_out] = drm_energy_dispersal(data_in)



%***************************************
%bemeno parameterek ellenorzese
if 1 > size(data_in,1) & size(data_in,2) ~= 1
    disp('data_in egy sorvektor');
    return;
end
        
    
%********************************************
%valtozok kezdoertekeinek beallitasa
data_out = [];
d0 = 1;
d1 = 1;
d2 = 1;
d3 = 1;
d4 = 1;
d5 = 1;
d6 = 1;
d7 = 1;
d8 = 1;

for I = 1:length(data_in)
    data_out(I) = rem((data_in(I) + d4 + d8),2);
    temp = rem((d4 + d8),2);
    d8 = d7;
    d7 = d6;
    d6 = d5;
    d5 = d4;
    d4 = d3;
    d3 = d2;
    d2 = d1;
    d1 = d0;
    d0 = temp;
end



return;



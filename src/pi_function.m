function new_values = pi_function(xin, tp)
    new_values(1) = 0;
    
    s = 2 ^ ceil(log2(xin));
    q = s/4 - 1;


    for I = 2:xin
        new_values(I) = rem((tp*new_values(I-1)+q),s);
        while new_values(I) >= xin
            new_values(I) = rem((tp*new_values(I)+q),s);
        end;
    end;
    
    return;

%		DRM cell mapper
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "OFDM cell mapper"
%es "pilot generator" egysegek feladatait latja el. Figyelembe veszi a 
%frekvencia, az ido es az amplitudo referencia cellak-at, ezeket 
%automatikusan a szabvanynak megfeleloen helyezi el a DRM jelben. A program 
%a DRM jel egy "superframe"-jenek cellakiosztasat szamitja ki.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%MSC_data_values:   A DRM jel egy super frame-jenek MSC cellait 
%           adja meg ez a
%			vektor. Azaz az MSC csatornabol erkezo adatokat tartalmazza.
%
%MSC_modulation_type:   Ez a valtozo adja meg az MSC csatorna modulacio tipusat. 
%           Erteke 16 vagy 64, ami rendre 16QAM
%			es 64QAM modulaciot jelentenek.
%
%SDC_data_values:   A DRM jel egy super frame-jenek SDC cellainak 
%           modulacios ertekeit adja meg ez a
%			vektor. Azaz az SDC csatornabol erkezo adatokat tartalmazza.
%			Megjegyzes: A program elso lepeskent ezen ertekek
%			SDC_modulation_type alapu modolusat veszi.
%
%FAC_data_values:   A DRM jel egy super frame-jenek FAC cellainak 
%           modulacios ertekeit adja meg ez a
%			vektor. Azaz az FAC csatornabol erkezo adatokat tartalmazza.
%			Megjegyzes: A program elso lepeskent ezen ertekek
%			4-es alapu modolusat veszi, mivel ez a csatorna mindeg 4QAM-el modulalt.
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%A bemeno parameterek alapjan a program a cellakat elhelyezi az ido frekvencia
%racson.
%
%
%
%Megjegyzes: A program egy contains() nevu segedfuggvenyt hasznal, amely
%feladata mindossze abbol all, hogy megmondja, hogy egy adott ertek 
%szerepel-e egy adott matrix adott soraban.
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%Ha MSC_modulation_type 64, akkor SDC_modulation_type csak 16 lehet, es
%ha MSC_modulation_type 16, akkor SDC_modulation_type csak 4 lehet.
%
%
%Minden frame-en belul 65 FAC cella van, tehaz egy super frame-ben 3*65.
%Azaz a FAC_data_values vektor elemeinek szama 3*65. 
%
%
%
%	MSC cella szam( MSC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	3777    4266    7896    8877    16392   18354
%	B	2898    3330    6153    7011    12747   14322
%	C	-       -       -       5532    -       11601
%	D	-       -       -       3678    -       7818
%
%
%
%
%	SDC cella szam( SDC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	167     190     359     405     754     846
%	B	130     150     282     322     588     662
%	C	-       -       -       288     -       607
%	D	-       -       -       152     -       332
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%
%
%



function [modulation_values] = drm_cell_mapper(MSC_data_values, MSC_modulation_type, SDC_data_values, FAC_data_values, robustness_mode, spectrum_occupancy)



%***************************************
%bemeno parameterek ellenorzese
modulation_values = [];

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end

if size(MSC_modulation_type) ~= 1 
	disp('MSC_modulation_type egy erteket vehet fel: 16 64');
	return;
end

if MSC_modulation_type ~= 16 & MSC_modulation_type ~= 64
	disp('MSC_modulation_type egy erteket vehet fel: 16 64');
	return;
end

QAM_MSC_cell_number_table = [3777 4266 7896 8877 16392 18354; 2898 3330 6153 7011 12747 14322; 0 0 0 5532 0 11601; 0 0 0 3678 0 7818];

QAM_MSC_cell_number = QAM_MSC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

if size(MSC_data_values, 2) ~= QAM_MSC_cell_number | size(MSC_data_values, 1) ~= 1
	disp(sprintf('MSC_data_values-nak egy sorvektornak kell lennie %d elemmel', QAM_MSC_cell_number));
	return;
end


QAM_SDC_cell_number_table = [167 190 359 405 754 846; 130 150 282 322 588 662; 0 0 0 288 0 607; 0 0 0 152 0 332];

QAM_SDC_cell_number = QAM_SDC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

if size(SDC_data_values, 2) ~= QAM_SDC_cell_number | size(SDC_data_values, 1) ~= 1
	disp(sprintf('SDC_data_values-nak egy sorvektornak kell lennie %d elemmel', QAM_SDC_cell_number));
	return;
end


QAM_FAC_cell_number = 3*65;

if size(FAC_data_values, 2) ~= QAM_FAC_cell_number | size(FAC_data_values, 1) ~= 1
	disp(sprintf('FAC_data_values-nak egy sorvektornak kell lennie %d elemmel', QAM_FAC_cell_number));
	return;
end



%********************************************
%valtozok kezdoertekeinek beallitasa

if MSC_modulation_type == 16
	a_MSC = 1/sqrt(10);
elseif MSC_modulation_type == 64
	a_MSC = 1/sqrt(42);
end

%cell loss per super frame - ha kell hozzaadok a bemeno adatokhoz "dummy"
%cellakat
NL_table = [1 2 1 0 2 0; 2 0 0 2 0 1; 0 0 0 0 0 2; 0 0 0 1 0 1];

NL = NL_table(robustness_mode+1, spectrum_occupancy+1);

if NL ~= 0
    z = a_MSC*(1 + j);
    MSC_data_values(length(MSC_data_values)+1) = z;
end

if NL == 2
    z = a_MSC*(1 - j);
    MSC_data_values(length(MSC_data_values)+1) = z;
end
   
Ns_table = [15 15 20 24];

Ns = Ns_table(robustness_mode+1);

%For robustness modes A and B, the SDC symbols are symbols 0 and 1 of each transmission super frame. For robustness
%modes C and D, the SDC symbols are symbols 0, 1 and 2 of each transmission super frame.

if robustness_mode >= 2 
    SDC_symbols = [0 1 2];
else
    SDC_symbols = [0 1];
end

if robustness_mode == 0
	W1024 = [228 341 455; 455 569 683; 683 796 910; 910 0 114; 114 228 341];
	Z256 = [0 81 248; 18 106 106; 122 116 31; 129 129 39; 33 32 111];
	Q1024 = 36;
elseif robustness_mode == 1
	W1024 = [512 0 512 0 512; 0 512 0 512 0; 512 0 512 0 512];
	Z256 = [0 57 164 64 12; 168 255 161 106 118; 25 232 132 233 38];
	Q1024 = 12;
elseif robustness_mode == 2
	W1024 = [465 372 279 186 93 0 931 838 745 652; 931 838 745 652 559 465 372 279 186 93];
	Z256 = [0 76 29 76 9 190 161 248 33 108; 179 178 83 253 127 105 101 198 250 145];
	Q1024 = 12;
elseif robustness_mode == 3
	W1024 = [366 439 512 585 658 731 805 878; 731 805 878 951 0 73 146 219; 73 146 219 293 366 439 512 585];
	Z256 = [0 240 17 60 220 38 151 101; 110 7 78 82 175 150 106 25; 165 7 252 124 253 177 197 142];
	Q1024 = 14;
end


%k_base vektor feltoltese

if spectrum_occupancy == 0 & robustness_mode == 0
	k_base = [2:102];
	power_boosted_carrier = [2 6 98 102];
elseif spectrum_occupancy == 1 & robustness_mode == 0
	k_base = [2:114];
	power_boosted_carrier = [2 6 110 114];
elseif spectrum_occupancy == 2 & robustness_mode == 0
	k_base = [-102:-2 2:102];
	power_boosted_carrier = [-102 -98 98 102];
elseif spectrum_occupancy == 3 & robustness_mode == 0
	k_base = [-114:-2 2:114];
	power_boosted_carrier = [-114 -110 110 114];
elseif spectrum_occupancy == 4 & robustness_mode == 0
	k_base = [-98:-2 2:314];
	power_boosted_carrier = [-98 -94 310 314];
elseif spectrum_occupancy == 5 & robustness_mode == 0
	k_base = [-110:-2 2:350];
	power_boosted_carrier = [-110 -106 346 350];
elseif spectrum_occupancy == 0 & robustness_mode == 1
	k_base = [1:91];
	power_boosted_carrier = [1 3 89 91];
elseif spectrum_occupancy == 1 & robustness_mode == 1
	k_base = [1:103];
	power_boosted_carrier = [1 3 101 103];
elseif spectrum_occupancy == 2 & robustness_mode == 1
	k_base = [-91:-1 1:91];
	power_boosted_carrier = [-91 -89 89 91];
elseif spectrum_occupancy == 3 & robustness_mode == 1
	k_base = [-103:-1 1:103];
	power_boosted_carrier = [-103 -101 101 103];
elseif spectrum_occupancy == 4 & robustness_mode == 1
	k_base = [-87:-1 1:279];
	power_boosted_carrier = [-87 -85 277 279];
elseif spectrum_occupancy == 5 & robustness_mode == 1
	k_base = [-99:-1 1:311];
	power_boosted_carrier = [-99 -97 309 311];
elseif spectrum_occupancy == 3 & robustness_mode == 2
	k_base = [-69:-1 1:69];
	power_boosted_carrier = [-69 -67 67 69];
elseif spectrum_occupancy == 5 & robustness_mode == 2
	k_base = [-67:-1 1:213];
	power_boosted_carrier = [-67 -65 211 213];
elseif spectrum_occupancy == 3 & robustness_mode == 3
	k_base = [-44:-1 1:44];
	power_boosted_carrier = [-44 -43 43 44];
elseif spectrum_occupancy == 5 & robustness_mode == 3
	k_base = [-43:-1 1:135];
	power_boosted_carrier = [-43 -42 134 135];
end


%k_fr vektor, azaz a frekvencia referencia cellak vivoi
%(elso sor a vivo sorszama, masodik sor fazis)
k_fr_table = [18 54 72 16 48 64 11 33 44 7 21 28; 205 836 215 331 651 555 214 392 242 788 1014 332];
k_fr = k_fr_table(1:2,robustness_mode*3+1:robustness_mode*3+3);


%az osszes eddigi k vektorok valtozatlanul 
%maradnak a frame feldolgozasa folyaman
%az ezutan kovetkezok minden s es m ertekenel elteroek


%********************************************
frame_counter = 1;
while frame_counter <= 3
    
    s = 0;
    while s < Ns


%k_t vektor, azaz az ido referencia cellak vivoi
%(elso sor a vivo sorszama, masodik sor fazis)
	    k_t = [];
    	if s == 0
    		if robustness_mode == 0
    			k_t = [17 19 21 28 29 32 33 39 40 41 53 55 56 60 61 63 71 73; 973 717 264 357 357 952 440 856 88 88 68 836 836 1008 1008 752 215 727];
    		elseif robustness_mode == 1
    			k_t = [14 18 20 24 26 32 36 42 44 49 50 54 56 62 66 68; 304 108 620 192 704 44 432 588 844 651 651 460 460 944 940 428];
    		elseif robustness_mode == 2
    			k_t = [8 10 12 14 16 18 22 24 28 30 32 36 38 42 45 46; 722 466 214 479 516 260 577 662 3 771 392 37 37 474 242 754];
    		elseif robustness_mode == 3
    			k_t = [5 6 8 9 11 12 14 15 17 18 20 23 24 26 27 29 30 32; 636 124 788 200 688 152 920 920 644 388 652 176 176 752 496 432 964 452];
    		end
    	end

%k_g vektor, azaz az amplitudo referencia cellak vivoi
%(egysoros vektor)
%(elso sor a vivo sorszama)

    	k_g = [];
    	if robustness_mode == 0
    		p_max = floor((k_base(length(k_base)) - 2 - 4 * rem(s, 5))/20);
    		p_min = ceil((k_base(1) - 2 - 4 * rem(s, 5))/20);

    		for p = p_min:p_max,
    			new_value = 2 + 4 * rem(s, 5) + 20 * p;			

    			if ~(contains(new_value,k_fr,1) | contains(new_value,k_t,1))
    				k_g(length(k_g)+1) = new_value;
    			end
    		end
    	elseif robustness_mode == 1
    		p_max = floor((k_base(length(k_base)) - 1 - 2 * rem(s, 3))/6);
    		p_min = ceil((k_base(1) - 1 - 2 * rem(s, 3))/6);

    		for p = p_min:p_max,
    			new_value = 1 + 2 * rem(s, 3) + 6 * p;			

    			if ~(contains(new_value,k_fr,1) | contains(new_value,k_t,1))
    				k_g(length(k_g)+1) = new_value;

    			end
    		end
    	elseif robustness_mode == 2
    		p_max = floor((k_base(length(k_base)) - 1 - 2 * rem(s, 2))/4);
    		p_min = ceil((k_base(1) - 1 - 2 * rem(s, 2))/4);

    		for p = p_min:p_max,
    			new_value = 1 + 2 * rem(s, 2) + 4 * p;			

    			if ~(contains(new_value,k_fr,1) | contains(new_value,k_t,1))
    				k_g(length(k_g)+1) = new_value;

    			end
    		end
    	elseif robustness_mode == 3
    		p_max = floor((k_base(length(k_base)) - 1 - rem(s, 3))/3);
    		p_min = ceil((k_base(1) - 1 - rem(s, 3))/3);

    		for p = p_min:p_max,
    			new_value = 1 + rem(s, 3) + 3 * p;			

    			if ~(contains(new_value,k_fr,1) | contains(new_value,k_t,1))
    				k_g(length(k_g)+1) = new_value;

    			end
    		end
    	end

    
        k_SDC = [];
        k_FAC = [];  
        k_MSC = [];  

        if frame_counter == 1 & contains(s,SDC_symbols,1)

%k_SDC vektor, azaz ezeket a megmaradt vivoket hasznalhatjuk adatatvitelre
%(elso sor a vivo sorszama)

        	k_SDC = [];
        	for I = 1:length(k_base)
        		if ~(contains(k_base(I),k_fr,1) | contains(k_base(I),k_t,1) | contains(k_base(I),k_g,1))
        			k_SDC(size(k_SDC,2)+1) = k_base(I);
        		end
        	end
        else
%k_FAC vektor, ezeket a vivoket hasznalhatjuk adatatvitelre
%(elso sor a vivo sorszama)
    
            k_FAC = [];  
            if robustness_mode == 0
                switch s
                    case 2
                        k_FAC = [26 46 66 86];
                    case 3
                        k_FAC = [10 30 50 70 90];
                    case 4
                        k_FAC = [14 22 34 62 74 94];
                    case 5
                        k_FAC = [26 38 58 66 78];
                    case 6
                        k_FAC = [22 30 42 62 70 82];
                    case 7
                        k_FAC = [26 34 46 66 74 86];
                    case 8
                        k_FAC = [10 30 38 50 58 70 78 90];
                    case 9
                        k_FAC = [14 22 34 42 62 74 82 94];
                    case 10
                        k_FAC = [26 38 46 66 86];
                    case 11
                        k_FAC = [10 30 50 70 90];
                    case 12
                        k_FAC = [14 34 74 94];
                    case 13
                        k_FAC = [38 58 78];
                end                
            elseif robustness_mode == 1
                switch s
                    case 2
                        k_FAC = [13 25 43 55 67];
                    case 3
                        k_FAC = [15 27 45 57 69];
                    case 4
                        k_FAC = [17 29 47 59 71];
                    case 5
                        k_FAC = [19 31 49 61 73];
                    case 6
                        k_FAC = [9  21 33 51 63 75];
                    case 7
                        k_FAC = [11 23 35 53 65 77];
                    case 8
                        k_FAC = [13 25 37 55 67 79];
                    case 9
                        k_FAC = [15 27 39 57 69 81];
                    case 10
                        k_FAC = [17 29 41 59 71 83];
                    case 11
                        k_FAC = [19 31 43 61 73];
                    case 12
                        k_FAC = [21 33 45 63 75];
                    case 13
                        k_FAC = [23 35 47 65 77];        
                end                
            elseif robustness_mode == 2
                switch s
                    case 3
                        k_FAC = [9  21 45 57];
                    case 4
                        k_FAC = [23 35 47];
                    case 5
                        k_FAC = [13 25 37 49];
                    case 6
                        k_FAC = [15 27 39 51];
                    case 7
                        k_FAC = [5  17 29 41 53];
                    case 8
                        k_FAC = [7  19 31 43 55];
                    case 9
                        k_FAC = [9  21 45 57];
                    case 10
                        k_FAC = [23 35 47];
                    case 11
                        k_FAC = [13 25 37 49];
                    case 12
                        k_FAC = [15 27 39 51];
                    case 13
                        k_FAC = [5  17 29 41 53];
                    case 14
                        k_FAC = [7  19 31 43 55];
                    case 15
                        k_FAC = [9  21 45 57];
                    case 16
                        k_FAC = [23 35 47];
                    case 17
                        k_FAC = [13 25 37 49];
                    case 18
                        k_FAC = [15 27 39 51];
                end                
            else    
                switch s
                    case 3
                        k_FAC = [9  18 27];
                    case 4
                        k_FAC = [10 19];
                    case 5
                        k_FAC = [11 20 29];
                    case 6
                        k_FAC = [12 30];
                    case 7
                        k_FAC = [13 22 31];
                    case 8
                        k_FAC = [5  14 23 32];
                    case 9
                        k_FAC = [6  15 24 33];
                    case 10
                        k_FAC = [16 25 34];
                    case 11
                        k_FAC = [8 17 26 35];
                    case 12
                        k_FAC = [9 18 27 36];
                    case 13
                        k_FAC = [10 19 37];
                    case 14
                        k_FAC = [11 20 29];
                    case 15
                        k_FAC = [12 30];
                    case 16
                        k_FAC = [13 22 31];
                    case 17
                        k_FAC = [5  14 23 32];
                    case 18
                        k_FAC = [6  15 24 33];
                    case 19
                        k_FAC = [16 25 34];
                    case 20
                        k_FAC = [8  17 26 35];
                    case 21
                        k_FAC = [9  18 27 36];
                    case 22
                        k_FAC = [10 19 37];
                end                
            end
    
%k_MSC vektor, azaz ezeket a megmaradt vivoket hasznalhatjuk adatatvitelre
%(elso sor a vivo sorszama)
        
    	    k_MSC = [];
        	for I = 1:length(k_base)
        		if ~(contains(k_base(I),k_fr,1) | contains(k_base(I),k_t,1) | contains(k_base(I),k_g,1) | contains(k_base(I),k_FAC,1))
        			k_MSC(size(k_MSC,2)+1) = k_base(I);
        		end
        	end
        end 


        row_number = (frame_counter-1)*Ns+s+1;

%A kiemenet szamitasa
        for I = 1:size(k_base,2)

            index = contains(k_base(I),k_fr,1);
            if index
        	    c=sqrt(2);
        
        		theta = k_fr(2,index);
        		if (robustness_mode == 3) & (rem(s,2)) & (k_fr(1,index) == 7 | k_fr(1,index) == 21)
        			theta = rem(theta + 512, 1024);
        		end
        		U=exp(j*2*pi*theta/1024);
   
                modulation_values(row_number,I) = c*U;
            end

            index = contains(k_base(I),k_t,1);
            if index
        		c=sqrt(2);

                theta = k_t(2,index);
        		U=exp(j*2*pi*theta/1024);

                modulation_values(row_number,I) = c*U;
         
            end

            index = contains(k_base(I),k_g,1);
            if index
        	    xyk0 = [4 5 2; 2 3 1; 2 2 1; 1 3 1];

        	    x = xyk0(robustness_mode+1, 1);
                y = xyk0(robustness_mode+1, 2);
        	    k0 = xyk0(robustness_mode+1, 3);

        	    n = rem(s,y);
        	    m = floor(s/y);

                p=(k_g(index)-k0-n*x)/(x*y);
        		theta = rem(4*Z256(n+1,m+1)+p*W1024(n+1,m+1)+p*p*Q1024*(1+s),1024);
        		U=exp(j*2*pi*theta/1024);

                c = sqrt(2);
        		if contains(k_g(index),power_boosted_carrier,1) 
        			c=2;
        		end

                modulation_values(row_number,I) = c*U;
        
            end
            
            if contains(k_base(I),k_FAC,1)
                
                modulation_values(row_number,I) = FAC_data_values(1);
                FAC_data_values = FAC_data_values(2:end);
            
            elseif contains(k_base(I),k_SDC,1)
  
                modulation_values(row_number,I) = SDC_data_values(1);
                SDC_data_values = SDC_data_values(2:end);
            
            elseif contains(k_base(I),k_MSC,1)
                
                modulation_values(row_number,I) = MSC_data_values(1);
                MSC_data_values = MSC_data_values(2:end);
            end
        
        end
 
%kovetkezo OFDM szimbolum jon
    	s = s + 1;
    end

    frame_counter = frame_counter + 1;
end

%Ez a fajl a drm_cell_mapper es drm_OFDM programok hasznalatat mutatja be peldan keresztul.
%Ezen programoknak sok bemeno parametere van, es ezeket a drm szabvany erosen koti is. Pl.:
%	MSC cella szam( SDC_data_values vektor elemeinek szama)
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	3777    4266    7896    8877    16392   18354
%	B	2898    3330    6153    7011    12747   14322
%	C	-       -       -       5532    -       11601
%	D	-       -       -       3678    -       7818
%
%
%	SDC cella szam( SDC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	167     190     359     405     754     846
%	B	130     150     282     322     588     662
%	C	-       -       -       288     -       607
%	D	-       -       -       152     -       332
%
%
%spectrum_occupancy     0       1       2       3       4       5	
%savszelesseg [Hz]      4500    5000    9000    10000   18000  20000
%
%
%Ez a fajl eleget tesz a felteteleknek, es igy nem kell kezzel
%megadni minden bemeno parametert a drm_cell_mapper es drm_OFDM
%fuggvenyeknek, ha pusztan csak egy peldat szeretnenk latni.
%Eleg csak az alabbi programreszt egy "%Szamitas:" komment utan masolni:
%
%    [modulation_values] = drm_cell_mapper(MSC_data_values, MSC_modulation_type, SDC_data_values, SDC_modulation_type, FAC_data_values, robustness_mode, spectrum_occupancy);
%    disp('Komplex ertekek kiszamitva.');
%    disp('Elso frame, elso harom szimbolumanak szamitasa:');
%    modulation_values = modulation_values(1:3,:);
%    [time_domain,time] = drm_OFDM(robustness_mode, spectrum_occupancy, modulation_values, oversampling_rate, carrier_frequency);
%
%The constellation should be chosen with respect to the MSC parameters to provide more robustness for the SDC than
%for the MSC. When using hierarchical modulation, the SDC shall be coded using 4-QAM.

interleaving_length = 0;

MSC_modulation_type = 64;


%oversampling_rate = 4;
oversampling_rate = 3.2;

carrier_frequency = 10000;

%	A	3777    4266    7896    8877    16392   18354
%	A	167     190     359     405     754     846
robustness_mode = 'A';

spectrum_occupancy = 3;
%SDC 16 QAM modulacioja eseten az Rall (kodolasi arany) 0,5, amivel a fuggelekekben
%kikeresheto az idevago sor(SM kodolas EEP, A robustness mode!):
%Rall		spectrum_occupancy
%
%		0       1       2       3       4       5	
%		321     366     705     798     1494    1680
%Ebben az esetben LSDC = 798
MSC_protection_level = 0;
%Ilyenforman az Rall (kodolasi arany) 0,5, amivol a fuggelekekben
%kikeresheto az idevago sor(SM kodolas EEP, A robustness mode!):
%Rall		spectrum_occupancy
%
%		0       1       2       3       4       5	
%		3757    4248    7878    8857    16374   18336
%Ebben az esetben LMSC = 8857



disp('Kiindulo ertekek megadasa.');
%MSC csatorna bitjeivel egyelore nem foglalkozok
MSC_data_values=zeros(1,8857);
% Az SDC data field-nek 97 byte-osnak kell lennie
%
% SDC_data_values=[0 0 0 0 <- indicates the number of transmission super
% frames which separate ths SDC block from the next with identical content
%
%   Innentol data field bitek
%   1. data entity header:
%     0 0 0 0 0 1 1 <- length of the body (number of whole bytes in data entity body)
%     0 <- the following data is for the current cunfiguration the (version
%     flag)
%     0 0 0 0 <- data entity type (currently: Multiplex description data entity)
%   data entity body:
%     0 0 <- protection level for part A
%     0 0 <- protection level for part B
%      stream description for stream 0
%     0 0 0 0 0 0 0 0 0 0 0 0 <- data length for part A
%     0 0 0 0 0 0 0 0 0 0 0 1 <- data length for part B 
%   2. data entity header:
%     0 0 0 0 1 0 0 <- length of the body (number of whole bytes in data entity body)
%     0 <- the following data is for the current cunfiguration the (version
%     flag)
%     0 1 0 1 <- data entity type (currently: Application information data entity)
%   data entity body:
%     0 1 <- short ID
%     0 1 <- Stream ID
%     0 <- synchronous stream mode (packet mode indicator)
%     0 0 0 <- referred for future use, currently set to zero
%     0 <- no enhancement available
%     0 0 0 <- DRM (application domain)
%     0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 <- user application identifier
%     0 0 0 0 0 0 0 0<- application data field (s) as required by the corresponding DRM application specification
%
%   Ezzel keszen vagyok, minden adat kiirva, ugyhogy a tobbit padding-el
%   toltom ki
% 16bit + 3byte + 16bit + 4byte= 2byte +3byte +2byte + 4byte=5 + 6=  11 byte -Van eddig a data fieldben, de pontosan
% 97 byte-nak kell lennie, tehat 97-11=86 byte padding, azaz 688 bit
% 
%     zeros(1,688)];

 SDC_data_values=[0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 zeros(1,688)];


% FAC_data_values=[
%     Csatorna leirasa:  
%       0 <-Base Layer - decodable by all DRM receivers
% *********************************
% DREAM:
% case 0:
%     (*pbiFACData).Enqueue(3 /* 11 */, 2);
%     break;
% case 1:
%     (*pbiFACData).Enqueue(1 /* 01 */, 2);
%     break;
% case 2:
%     (*pbiFACData).Enqueue(2 /* 10 */, 2);
%     break;
%
%en is atirtam 0 0-rol 1 1-re
% ***********************************
%     1 1 <- first FAC of the transmission super frame
%     0 0 0 0 <- spectrum occupancy ?????????????????????? Hogyan vannak reprezentalva a szamertekek?
%     1 <- 400 ms (short interleaving)
%     0 0 <- 64-QAM, no hierarchical (MSC modulaciojanak tipusa)
%     0 <- 16-QAM (SDC modulaciojanak tipusa)
%     0 0 0 1 <- 1 data service (A szolgaltatasok tipusa)
%     0 0 0 <- (Rekonfiguracios index, a nem nulla ertek megadja azoknak super frameeknek a szamat, ami meg a regi konfiguracioval feldolgozando)
%     0 0 <- Reserved for future use, shell be set to zero
%   Csatorna leirasanak vege, Szolgaltatas leirasanak kezdete
%     0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 <- Service identifier  ??????????? Onkenyesen megadhato? (Itt onkenyes)
%     0 1 <- short identifier, used as a reference in the SDC
%     0 <- No CA system is used for the audio stream(or the service has no audio service)
%     0 0 0 0 <- this 4-bit field indicates the language of the target (currently: No language specified)
%     1 <- Data service
%DREAM service descriptor neven hivatkozik erre az 5 bitre
%     0 0 0 0 0 <- Application identifier (currently: application is only signalled in the SDC)
%DREAM a kovetkezo 7 bitre Rfa kent hivatkozik (valoszinuleg rfu-z akartak irni)
%     0 <- No CA system is used for the data stream/sub-stream (or the service has no stream/sub-stream)
%     0 0 0 0 0 0]; <- reserved for future, shall be set to zero

FAC_data_values_1=[0 1 1 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0];
FAC_data_values_2=[0 0 1 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0];
FAC_data_values_3=[0 1 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0];

%Szamitas:

disp('CRC szmitas.');
disp('FAC');
FAC_CRC_word_1 = drm_FAC_pre_coder_CRC_word(FAC_data_values_1);
FAC_CRC_word_2 = drm_FAC_pre_coder_CRC_word(FAC_data_values_2);
FAC_CRC_word_3 = drm_FAC_pre_coder_CRC_word(FAC_data_values_3);
disp('SDC');
SDC_CRC_word = drm_SDC_pre_coder_CRC_word([0 0 0 0 SDC_data_values]);

%Azaz van 4 bit AFS index + 97 byte data field+ 16 bit CRC= 776 bit +
%16 bit + 4 bit= 796 bit
%De LSDC = 798 igy meg hozzaadok 2 bit paddinget az egeszhez
%
SDC_data_values = [SDC_data_values SDC_CRC_word 0 0];
FAC_data_values_1 = [FAC_data_values_1 FAC_CRC_word_1];
FAC_data_values_2 = [FAC_data_values_2 FAC_CRC_word_2];
FAC_data_values_3 = [FAC_data_values_3 FAC_CRC_word_3];

disp('energy dispersal.');
disp('FAC');
FAC_data_values_1 = drm_energy_dispersal(FAC_data_values_1);
FAC_data_values_2 = drm_energy_dispersal(FAC_data_values_2);
FAC_data_values_3 = drm_energy_dispersal(FAC_data_values_3);
disp('SDC');
SDC_data_values = drm_energy_dispersal(SDC_data_values);
disp('MSC');
MSC_data_values = drm_energy_dispersal(MSC_data_values);

disp('csatorna kodolas.');
disp('FAC');
FAC_data_values_1 = drm_FAC_4_SM_EEP_encoder(FAC_data_values_1);
FAC_data_values_2 = drm_FAC_4_SM_EEP_encoder(FAC_data_values_2);
FAC_data_values_3 = drm_FAC_4_SM_EEP_encoder(FAC_data_values_3);
disp('SDC');
SDC_data_values = drm_SDC_16_SM_EEP_encoder(SDC_data_values, robustness_mode, spectrum_occupancy);
disp('MSC');
MSC_data_values = drm_MSC_64_SM_EEP_encoder(MSC_data_values, MSC_protection_level, robustness_mode, spectrum_occupancy);

disp('cell interleaving for MSC.');
MSC_data_values = drm_interleaver(MSC_data_values, robustness_mode, spectrum_occupancy, interleaving_length);

disp('cell mapping.');
[modulation_values] = drm_cell_mapper([MSC_data_values MSC_data_values MSC_data_values], MSC_modulation_type, SDC_data_values, [FAC_data_values_1 FAC_data_values_2 FAC_data_values_3], robustness_mode, spectrum_occupancy);
disp('time domian computing');
disp('1/3 frame');
[time_domain_1,time_domain_1_2,time_1,sampling_fr] = drm_OFDM2(robustness_mode, spectrum_occupancy, modulation_values(1:15,:), oversampling_rate, carrier_frequency);
disp('2/3 frame');
[time_domain_2,time_domain_2_2,time_2,sampling_fr] = drm_OFDM2(robustness_mode, spectrum_occupancy, modulation_values(16:30,:), oversampling_rate, carrier_frequency);
disp('3/3 frame');
[time_domain_3,time_domain_3_2,time_3,sampling_fr] = drm_OFDM2(robustness_mode, spectrum_occupancy, modulation_values(31:end,:), oversampling_rate, carrier_frequency);

disp('.wav irasa');
wavwrite([time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3 time_domain_1 time_domain_2 time_domain_3]/100,sampling_fr,16,'drm2.wav');
disp('.wav irasa2');
wavwrite([time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2 time_domain_1_2 time_domain_2_2 time_domain_3_2]/100,sampling_fr,16,'drm22.wav');

disp('.dat irasa');
dcu214_write_DA_data2('drm2.dat', [time_domain_1 time_domain_2 time_domain_3], sampling_fr, floor(size(time_domain_1,2)), 1);
disp('.dat irasa2');
dcu214_write_DA_data2('drm22.dat', [time_domain_1_2 time_domain_2_2 time_domain_3_2], 48000, floor(size(time_domain_1_2,2)), 1);

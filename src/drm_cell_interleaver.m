%		DRM cell interleaver
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "cell interleaver"
%egyseg feladatait latja el. A kapott cellak sorrendjet a definialt modon valtoztatja meg.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%MSC_data_values:   A DRM jel egy frame-jenek MSC cellainak 
%           modulacios ertekeit adja meg ez a
%			vektor. Azaz az MSC csatornabol erkezo mar kodolt adatokat tartalmazza.
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%interleaving_length:   Az interleaving kiterjedeset hatarozza meg. 0
%           eseten csak egy frame-nek a cellait dolgozza fel, 1 eseten 5
%           egymast koveto frame cellai kerulnek osszekeveresre.
%
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%	MSC cella szam( MSC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	1259    1422    2632    2959    5464    6118
%	B	966     1110    2051    2337    4249    4774
%	C	-       -       -       1844    -       3867
%	D	-       -       -       1226    -       2606
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%
%
%



function [new_MSC_data_values] = drm_cell_interleaver(MSC_data_values, robustness_mode, spectrum_occupancy, interleaving_length)



%***************************************
%bemeno parameterek ellenorzese
new_MSC_data_values = [];

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end

interleaving_length = rem(abs(floor(interleaving_length)), 2);

QAM_MSC_cell_number_table = [1259 1422 2632 2959 5464 6118; 966 1110 2051 2337 4249 4774; 0 0 0 1844 0 3867; 0 0 0 1226 0 2606];

NMUX = QAM_MSC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

if ~interleaving_length
    if size(MSC_data_values, 2) ~= NMUX | size(MSC_data_values, 1) ~= 1
	    disp(sprintf('MSC_data_values-nak egy sorvektornak kell lennie %d elemmel', NMUX));
	    return;
    end
else
    D = 5;
    
    if size(MSC_data_values, 2) ~= NMUX | size(MSC_data_values, 1) ~= D
	    disp(sprintf('MSC_data_values-nak egy %d soros matrixnak kell lennie %d oszloppal', D, NMUX));
	    return;
    end
end
    

%********************************************
%szamitasok
new_values = pi_function(NMUX) + 1;

if ~interleaving_length
    for I = 1:NMUX
        new_MSC_data_values(I) = MSC_data_values(new_values(I));
    end
else
    for I = 1:NMUX
        new_MSC_data_values(I) = MSC_data_values(rem(I-1,D)+1,new_values(I));
    end
end

return;


%Foprogram vege
%********************************************
%Seged fuggvenyek



function new_values = pi_function(NMUX)
    new_values = [];
    
    s = 2 ^ ceil(log2(NMUX));
    q = s/4 - 1;
    t0 = 5;

    for I = 2:NMUX
        new_values(I) = rem((t0*new_values(I-1)+q),s);
        while new_values(I) > NMUX
            new_values(I) = rem((t0*new_values(I)+q),s);
        end;
    end;
    
    return;
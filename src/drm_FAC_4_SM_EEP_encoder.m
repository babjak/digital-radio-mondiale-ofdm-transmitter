%		DRM 4QAM SM EEP encoder for FAC
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "channel encoder"
%egyseg feladatait latja el a FAC csatornan. A beerkezo biteket kodolja, 
%"megkeveri" majd a definialt konstellacios diagramm alapjan komplex erteku
%cellakat szamit.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%data_in:   A FAC csatorna adatbitjeit tartalmazo vektor.
%
%
%A bemeno parameterek alapjan a program a FAC csatorna adatait kodolja.
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%   Pontosan 72 bitet fogad a program
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%


function [data_out] = drm_FAC_4_SM_EEP_encoder(data_in)

%********************************************
%valtozok kezdoertekeinek beallitasa
data_out = [];

R_table = [3/5];
Rp = R_table(1:1);

NFAC = 65;

p_max = 1;

N1 = 0;
N2 = NFAC;


%L1=0;
%for I=1:p_max
%	L1 = L1 + 2*N1*Rp(I);
%end

%L2=0;
%for I=1:p_max
%	L2 = L2 + RXp*floor((2*N2-12)/RYp);
%end

%for I=1:p_max
%	M(I,2) = RXp(I)*floor((2*N2-12)/RYp(I));
%end

if size(data_in, 2) ~= 72  | size(data_in, 1) ~= 1
   disp(sprintf('data_in-nak egy sorvektornak kell lennie %d elemmel', 72));
   return;
end

data_in = rem(data_in, 2);

a = 1 / sqrt(2);


%********************************************
%szamitasok

%particionalas es kodolas
%p=0
v0 = encoder(data_in(1:end), 1, 0, Rp(1), 0); 

%interleaving
%p=0
y0 = interleaving(v0, 21);



while length(y0)
	temp1 = y0(1)*1;
    temp2 = y0(2)*1;

	data_out(end+1) = (1-2*temp1)*a + (1-2*temp2)*a*j;
   	y0 = y0(3:end);
end

return;

%Foprogram vege
%********************************************
%Seged fuggvenyek


function data_out = interleaving(data_in, tp)
	new_values = pi_function(length(data_in), tp) + 1;
	for I = 1:length(data_in)
        data_out(I) = data_in(new_values(I));
	end

	return

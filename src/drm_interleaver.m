%		DRM interleaver
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "OFDM cell mapper"
%es "pilot generator" egysegek feladatait latja el. Figyelembe veszi a 
%frekvencia, az ido es az amplitudo referencia cellak-at, ezeket 
%automatikusan a szabvanynak megfeleloen helyezi el a DRM jelben. A program 
%a DRM jel egy "superframe"-jenek cellakiosztasat szamitja ki.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%MSC_data_values:   A DRM jel egy super frame-jenek MSC cellainak 
%           modulacios ertekeit adja meg ez a
%			vektor. Azaz az MSC csatornabol erkezo adatokat tartalmazza.
%			Megjegyzes: A program elso lepeskent ezen ertekek
%			MSC_modulation_type alapu modolusat veszi. Tehat peldaul
%			MSC_modulation_type = 16 es MSC_data_value = 
%			[15 31 65...] akkor a moduluskepzes utan (15 mod 16,
%			31 mod 16, 65 mod 16) az uj MSC_data_value vektor 
%			erteke [15 15 1...] lesz, mig MSC_modulation_type = 64 eseten
%			[15 31 1...].
%
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%A bemeno parameterek alapjan a program a cellakat elhelyezi az ido frekvencia
%racson.
%
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%
%
%	MSC cella szam( MSC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	3777    4266    7896    8877    16392   18354
%	B	2898    3330    6153    7011    12747   14322
%	C	-       -       -       5532    -       11601
%	D	-       -       -       3678    -       7818
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%
%
%



function [new_MSC_data_values] = drm_interleaver(MSC_data_values, robustness_mode, spectrum_occupancy, interleaving_length)



%***************************************
%bemeno parameterek ellenorzese
new_MSC_data_values = [];

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end

interleaving_length = rem(interleaving_length, 2);

QAM_MSC_cell_number_table = [1259 1422 2632 2959 5464 6118; 966 1110 2051 2337 4249 4774; 0 0 0 1844 0 3867; 0 0 0 1226 0 2606];

NMUX = QAM_MSC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

if ~interleaving_length
    if size(MSC_data_values, 2) ~= NMUX | size(MSC_data_values, 1) ~= 1
	    disp(sprintf('MSC_data_values-nak egy sorvektornak kell lennie %d elemmel', NMUX));
	    return;
    end
else
    D = 5;
    
    if size(MSC_data_values, 2) ~= NMUX | size(MSC_data_values, 1) ~= D
	    disp(sprintf('MSC_data_values-nak egy %d soros matrixnak kell lennie %d oszloppal', D, NMUX));
	    return;
    end
end
    
%********************************************
%valtozok kezdoertekeinek beallitasa

%********************************************
%szamitasok
new_values = pi_function(NMUX,5) + 1;

if ~interleaving_length
    for I = 1:NMUX
        new_MSC_data_values(I) = MSC_data_values(new_values(I));
    end
else
    for I = 1:NMUX
        new_MSC_data_values(I) = MSC_data_values(rem(I-1,D)+1,new_values(I));
    end
end

return;


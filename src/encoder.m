function data_out = encoder(data_in, is_FAC, change_tailbits, Rp, rp)
	data_out = [];

 	switch Rp
 		case 1/4	
			Puncturing_matrix = [1; 1; 1; 1];
		case 3/10
			Puncturing_matrix = [1 1 1; 1 1 1; 1 1 1; 1 0 0];
		case 1/3     
        		Puncturing_matrix = [1; 1; 1; 0];
		case 4/11 
        		Puncturing_matrix = [1 1 1 1; 1 1 1 1; 1 1 1 0; 0 0 0 0];
		case 1/2 
        		Puncturing_matrix = [1; 1; 0; 0];
		case 4/7
        		Puncturing_matrix = [1 1 1 1; 1 0 1 0; 0 1 0 0; 0 0 0 0];
        
		case 3/5
        		Puncturing_matrix = [1 1 1; 1 0 1; 0 0 0; 0 0 0];
		case 2/3
        		Puncturing_matrix = [1 1; 1 0; 0 0; 0 0];
		case 8/11
        		Puncturing_matrix = [1 1 1 1 1 1 1 1; 1 0 0 1 0 0 1 0; 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0];
		case 3/4
        		Puncturing_matrix = [1 1 1; 1 0 0; 0 0 0; 0 0 0];
		case 4/5
        		Puncturing_matrix = [1 1 1 1; 1 0 0 0; 0 0 0 0; 0 0 0 0];
		case 7/8
        		Puncturing_matrix = [1 1 1 1 1 1 1; 1 0 0 0 0 0 0; 0 0 0 0 0 0 0; 0 0 0 0 0 0 0];
		case 8/9
        		Puncturing_matrix = [1 1 1 1 1 1 1 1; 1 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0];
	end
 
	old_length = length(data_in);
	data_in = [0 0 0 0 0 0 data_in 0 0 0 0 0 0];

	column = 1;
    
	for I = 7:length(data_in)
        if I == 7+old_length & ~is_FAC & change_tailbits

		switch rp
			case 0
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 0 0 0 0 0 0; 0 0 0 0 0 0];
    			case 1
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 0 0 0 0 0; 0 0 0 0 0 0];
    			case 2     
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 0 0 1 0 0; 0 0 0 0 0 0];
    			case 3 
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 0 1 0 0; 0 0 0 0 0 0];
    			case 4 
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 0 1 1 0; 0 0 0 0 0 0];
    			case 5
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 0 0 0 0 0 0];
        
    			case 6
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 0 0 0 0 0 0];
    			case 7
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 1 0 0 0 0 0];
    			case 8
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 1 0 0 1 0 0];
    			case 9
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 0 1 0 0];
    			case 10
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 0 1 0 1];
    			case 11
        			Puncturing_matrix = [1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 1 1; 1 1 1 1 0 1];
		end


		column = 1;
        end    


	    if Puncturing_matrix(1, column)
            data_out(end+1) = rem ((data_in(I) + data_in(I-2) + data_in(I-3) + data_in(I-5) + data_in(I-6)), 2);
        end
        
            
        if Puncturing_matrix(2, column)
            data_out(end+1) = rem ((data_in(I) + data_in(I-1) + data_in(I-2) + data_in(I-3) + data_in(I-6)), 2);
        end
   
        if Puncturing_matrix(3, column)
            data_out(end+1) = rem ((data_in(I) + data_in(I-1) + data_in(I-4) + data_in(I-6)), 2);
        end
   
        if Puncturing_matrix(4, column)
            data_out(end+1) = rem ((data_in(I) + data_in(I-2) + data_in(I-3) + data_in(I-5) + data_in(I-6)), 2);
        end

        
        column = rem (column, size(Puncturing_matrix, 2)) + 1;

         
        
    end
    
    return;
function return_value = contains(value, matrix, line_number)

return_value = 0;

if ~size(matrix)
	return;
end

if line_number > length(matrix(:,1)) | line_number < 1
	return;
end

for I = 1:length(matrix(line_number,:)),
	if matrix(line_number,I) == value
		return_value = I;
		return;
	end
end

return;
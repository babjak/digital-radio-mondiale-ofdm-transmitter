%Ez a fajl a drm_cell_mapper es drm_OFDM programok hasznalatat mutatja be peldan keresztul.
%Ezen programoknak sok bemeno parametere van, es ezeket a drm szabvany erosen koti is. Pl.:
%	MSC cella szam( SDC_data_values vektor elemeinek szama)
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	3777    4266    7896    8877    16392   18354
%	B	2898    3330    6153    7011    12747   14322
%	C	-       -       -       5532    -       11601
%	D	-       -       -       3678    -       7818
%
%
%	SDC cella szam( SDC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	167     190     359     405     754     846
%	B	130     150     282     322     588     662
%	C	-       -       -       288     -       607
%	D	-       -       -       152     -       332
%
%
%Ez a fajl eleget tesz a felteteleknek, es igy nem kell kezzel
%megadni minden bemeno parametert a drm_cell_mapper es drm_OFDM
%fuggvenyeknek, ha pusztan csak egy peldat szeretnenk latni.
%Eleg csak az alabbi programreszt egy "%Szamitas:" komment utan masolni:
%
%    [modulation_values] = drm_cell_mapper(MSC_data_values, MSC_modulation_type, SDC_data_values, SDC_modulation_type, FAC_data_values, robustness_mode, spectrum_occupancy);
%    disp('Komplex ertekek kiszamitva.');
%    disp('Elso frame, elso harom szimbolumanak szamitasa:');
%    modulation_values = modulation_values(1:3,:);
%    [time_domain,time] = drm_OFDM(robustness_mode, spectrum_occupancy, modulation_values, oversampling_rate, carrier_frequency);
%
%A valtoztatasok mentese utan egyszeru "drm_pelda" paranccsal megtekintheto
%egy pelda a drm jel-re.


oversampling_rate = 6;
carrier_frequency = 10000;

MSC_modulation_type = 64;
SDC_modulation_type = 16;

FAC_data_values = [1:195];



%	A	3777    4266    7896    8877    16392   18354
%	A	167     190     359     405     754     846
robustness_mode = 'A';

spectrum_occupancy = 0;

MSC_data_values=[1:3777];
SDC_data_values=[1:167];

%Szamitas:
    [modulation_values] = drm_cell_mapper(MSC_data_values, MSC_modulation_type, SDC_data_values, SDC_modulation_type, FAC_data_values, robustness_mode, spectrum_occupancy);
    disp('Komplex ertekek kiszamitva.');
    disp('Elso frame, elso harom szimbolumanak szamitasa:');
    modulation_values = modulation_values(1:3,:);
    [time_domain,time] = drm_OFDM(robustness_mode, spectrum_occupancy, modulation_values, oversampling_rate, carrier_frequency);


spectrum_occupancy = 1;

MSC_data_values=[1:4266];
SDC_data_values=[1:190];

%Szamitas:


spectrum_occupancy = 2;

MSC_data_values=[1:7896];
SDC_data_values=[1:359];

%Szamitas:


spectrum_occupancy = 3;

MSC_data_values=[1:8877];
SDC_data_values=[1:405];

%Szamitas:


spectrum_occupancy = 4;

MSC_data_values=[1:16392];
SDC_data_values=[1:754];

%Szamitas:


spectrum_occupancy = 5;

MSC_data_values=[1:18354];
SDC_data_values=[1:846];

%Szamitas:


%	B	2898    3330    6153    7011    12747   14322
%	B	130     150     282     322     588     662

robustness_mode = 'B';
spectrum_occupancy = 0;

MSC_data_values=[1:2898];
SDC_data_values=[1:130];

%Szamitas:


spectrum_occupancy = 1;

MSC_data_values=[1:3330];
SDC_data_values=[1:150];

%Szamitas:


spectrum_occupancy = 2;

MSC_data_values=[1:6153];
SDC_data_values=[1:282];

%Szamitas:


spectrum_occupancy = 3;

MSC_data_values=[1:7011];
SDC_data_values=[1:322];

%Szamitas:


spectrum_occupancy = 4;

MSC_data_values=[1:12747];
SDC_data_values=[1:588];

%Szamitas:


spectrum_occupancy = 5;

MSC_data_values=[1:14322];
SDC_data_values=[1:662];

%Szamitas:


%	C	-       -       -       5532    -       11601
%	C	-       -       -       288     -       607

robustness_mode = 'C';

spectrum_occupancy = 3;

MSC_data_values=[1:5532];
SDC_data_values=[1:288];

%Szamitas:


spectrum_occupancy = 5;

MSC_data_values=[1:11601];
SDC_data_values=[1:607];

%Szamitas:


%	D	-       -       -       3678    -       7818
%	D	-       -       -       152     -       332
robustness_mode = 'D';

spectrum_occupancy = 3;

MSC_data_values=[1:3678];
SDC_data_values=[1:152];

%Szamitas:


spectrum_occupancy = 5;

MSC_data_values=[1:7818];
SDC_data_values=[1:332];

%Szamitas:



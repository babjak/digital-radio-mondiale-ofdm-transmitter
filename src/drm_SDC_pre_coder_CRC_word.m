%		DRM cell interleaver
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "cell interleaver"
%egyseg feladatait latja el. A kapott cellak sorrendjet a definialt modon valtoztatja meg.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%MSC_data_values:   A DRM jel egy frame-jenek MSC cellainak 
%           modulacios ertekeit adja meg ez a
%			vektor. Azaz az MSC csatornabol erkezo mar kodolt adatokat tartalmazza.
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%interleaving_length:   Az interleaving kiterjedeset hatarozza meg. 0
%           eseten csak egy frame-nek a cellait dolgozza fel, 1 eseten 5
%           egymast koveto frame cellai kerulnek osszekeveresre.
%
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%	MSC cella szam( MSC_data_values vektor elemeinek szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0       1       2       3       4       5	
%	A	1259    1422    2632    2959    5464    6118
%	B	966     1110    2051    2337    4249    4774
%	C	-       -       -       1844    -       3867
%	D	-       -       -       1226    -       2606
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%
%
%



function [CRC_word] = drm_SDC_pre_coder_CRC_word(data_in)



%***************************************
%bemeno parameterek ellenorzese
CRC_word = [];

%********************************************
%szamitasok


CRC_word(1:16) = 1;

%    d0 = 1;
%    d1 = 1;
%    d2 = 1;
%    d3 = 1;
%    d4 = 1;
%    d5 = 1;
%    d6 = 1;
%    d7 = 1;

%x16 + x12 +x5 +x0
for I = 1:length(data_in)
    temp = rem((data_in(I)+CRC_word(1)),2);
    
    CRC_word(1) = CRC_word(2);
    CRC_word(2) = CRC_word(3);
    CRC_word(3) = CRC_word(4);
    CRC_word(4) = rem((temp+CRC_word(5)),2);
    CRC_word(5) = CRC_word(6);
    CRC_word(6) = CRC_word(7);
    CRC_word(7) = CRC_word(8);
    CRC_word(8) = CRC_word(9);
    CRC_word(9) = CRC_word(10);
    CRC_word(10) = CRC_word(11);
    CRC_word(11) = rem((temp+CRC_word(12)),2);
    CRC_word(12) = CRC_word(13);
    CRC_word(13) = CRC_word(14);
    CRC_word(14) = CRC_word(15);
    CRC_word(15) = CRC_word(16);
    CRC_word(16) = temp;

%    temp = rem((data_in(I)+d7),2);
    
%    d7 = d6;
%    d6 = d5;
%    d5 = d4;
%    d4 = rem((temp+d3),2);
%    d3 = rem((temp+d2),2);
%    d2 = rem((temp+d1),2);
%    d1 = d0;
%    d0 = temp;
end

for I = 1:16
    CRC_word(I) = rem((CRC_word(I)+1),2);
end

return;


%Foprogram vege
%********************************************
%Seged fuggvenyek

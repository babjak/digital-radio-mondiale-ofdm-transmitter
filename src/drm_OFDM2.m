%		DRM OFDM jel generator
%
%
%	Babjak Benjamin
%	2005.08.30	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "OFDM signal generator"
%es a "modulator" egysegek feladatait latja el. A program 
%a DRM jel egy "frame"-jet szamitja ki, vagy ha kevesebb bemeno adat all 
%rendelkezesere, akkor a frame "toredeket".
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%			
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%modulation_values:	A DRM jel cellainak komplex ertekeit adja meg ez a
%			matrix, az egy sorban levo ertekek egy idoben 
%			kerulnek feldolgozasra. Adott sorban elorebb levo ertekek
%           kisebb frekvenciaju segedvivon jelennek meg.
%
%oversampling_rate:	A program a bemeneti parameterbol kiszamitja a jelben
%			elofordulo legnagyobb frekvenciaju vivot. Az 
%			oversampling_rate hatarozza meg, hogy a legnagyobb
%			frekvencihoz kepest legalabb(!) hanyszor gyakrabban 
%			szam�tsa ki az OFDM jel idobeli alakjat. Tehat 
%			peldaul, ha a legnagyobb frekvencia 10kHz es az 
%			oversampling_rate erteke 4, akkor legalabb 40kHz 
%			gyakorisaggal szamit a program, azaz legfeljebb 0,025 
%			msec-onkent szamitja a jelalakot.
%
%carrier_frequency:	A program a kapott OFDM jelet ezen vivofrekvenciara fogja
%           elhelyezni. Erteket Hz-ben kell megadni.
%           Megjegyzes: A DRM szabvany sajatossagai miatt a fovivo frekvencia
%           es a legalacsonyabb frekvenciaju segedvivo kozott 5 kHz vagy 4,5kHZ-es
%           tavolsag van a spectrum_occupancy es a robustness_mode
%           fuggvenyeben. Ilyen forman annak erdekeben, hogy a
%           legalacsonyabb segedvivo is pozitiv frekvenciatartomanyra
%           essen a fo vivo frekvenciaja nem lehet kisebb 5 kHz nel.
%
%A bemeno parameterek alapjan a program az idobeli lefutast kiszamolja, es
%meg is jeleniti.
%
%
%Megjegyzes: A bemeno parameterkent megadott oversampling_rate ertek
%felhasznalasaval eloallhat olyan szamitasi periodusido, amely nem 
%egeszszamszor talalhato meg a szimbolum periodusidejeben. Azaz a 
%szamitasi ido periodicitasa csak egy szimbolum idon belul lenne garantalt
%az uj szimbolum kezdete nem feltetlenul esne ebbe a periodicitasba.
%Ennek elkerulese vegett a program ugy modositja az oversampling_rate
%erteket, hogy az a leheto legkozelebb essen a megadott ertekhez es ezzel
%egyidoben a minta szamitasi idejenek periodicitasa is teljesuljon.
%
%Megjegyzes: A program egy contains() nevu segedfuggvenyt hasznal, amely
%feladata mindossze abbol all, hogy megmondja, hogy egy adott ertek 
%szerepel-e egy adott matrix adott soraban.
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%	robustness_mode		OFDM szimbolum szam(modulation_values matrix sorainak szama maximum)
%
%	A			15
%	B			15
%	C			20
%	D			24
%
%
%	A vivo szam(a modulation_values matrix oszlopainak szama)
%
%
%	robustness_mode		spectrum_occupancy
%
%		0	1	2	3	4	5	
%	A	101	113	202	226	410	458
%	B	91	103	182	206	366	410
%	C	-	-	-	138	-	280		
%	D	-	-	-	88	-	178		
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.


function [time_domain, time_domain2, time,sampling_fr] = drm_OFDM2(robustness_mode, spectrum_occupancy, modulation_values, oversampling_rate, carrier_frequency)



%***************************************
%bemeno parameterek ellenorzese

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end


Ns_table = [15 15 20 24];

Ns = Ns_table(robustness_mode+1);

if size(modulation_values,1) > Ns
	disp('modulation_values sorainak szama tul sok');
	return;
end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end
	
%oversampling_rate = ceil(abs(oversampling_rate));
oversampling_rate = abs(oversampling_rate);

if 5000 > carrier_frequency
	disp('A carrier_frequency ertekenek nagyobbnak kell lenni 5000 Hz-nel');
	return;
end


%********************************************
%valtozok kezdoertekeinek beallitasa

time_domain = [];
time_domain2 = [];
time = [];

Ts_table = [0.0266666666 0.0266666666 0.0200000000 0.0166666666];

Ts = Ts_table(robustness_mode+1);

Tg_table = [0.0026666666 0.0053333333 0.0053333333 0.0073333333];

Tg = Tg_table(robustness_mode+1);

Tu_table = [0.0240000000 0.0213333333 0.0146666666 0.0093333333];

Tu = Tu_table(robustness_mode+1);


fr = carrier_frequency;


Max_freq = [4500 5000 4500 5000 13500 15000];

T_min = 1/((Max_freq(spectrum_occupancy + 1) + fr)* oversampling_rate);

T_min = Ts/ceil(Ts/T_min);

sampling_fr = 1 / T_min;

%k_base vektor feltoltese

if spectrum_occupancy == 0 & robustness_mode == 0
	k_base = [2:102];
elseif spectrum_occupancy == 1 & robustness_mode == 0
	k_base = [2:114];
elseif spectrum_occupancy == 2 & robustness_mode == 0
	k_base = [-102:-2 2:102];
elseif spectrum_occupancy == 3 & robustness_mode == 0
	k_base = [-114:-2 2:114];
elseif spectrum_occupancy == 4 & robustness_mode == 0
	k_base = [-98:-2 2:314];
elseif spectrum_occupancy == 5 & robustness_mode == 0
	k_base = [-110:-2 2:350];
elseif spectrum_occupancy == 0 & robustness_mode == 1
	k_base = [1:91];
elseif spectrum_occupancy == 1 & robustness_mode == 1
	k_base = [1:103];
elseif spectrum_occupancy == 2 & robustness_mode == 1
	k_base = [-91:-1 1:91];
elseif spectrum_occupancy == 3 & robustness_mode == 1
	k_base = [-103:-1 1:103];
elseif spectrum_occupancy == 4 & robustness_mode == 1
	k_base = [-87:-1 1:279];
elseif spectrum_occupancy == 5 & robustness_mode == 1
	k_base = [-99:-1 1:311];
elseif spectrum_occupancy == 3 & robustness_mode == 2
	k_base = [-69:-1 1:69];
elseif spectrum_occupancy == 5 & robustness_mode == 2
	k_base = [-67:-1 1:213];
elseif spectrum_occupancy == 3 & robustness_mode == 3
	k_base = [-44:-1 1:44];
elseif spectrum_occupancy == 5 & robustness_mode == 3
	k_base = [-43:-1 1:135];
end

%egy kicsit megkesett bemeno parameter hibaellenorzes
if length(k_base) ~= size(modulation_values,2)
	disp('modulation_values oszlopszama nem megfelelo');
	disp(length(k_base));
	return;
end	


%********************************************
%az idotartomany szamitasa

s = 0;
while s < size(modulation_values,1)
%____________________________________________________
    
% f_lepes = 1/Tu;
% t_lepes = 1/48000;
% 

%______________________

N=floor(48000*Tu);
guard_time_sample_number = round(N * Tg / Tu);
%	DC_index = floor( 6000 * RMA_FFT_SIZE_N / 48000);

%teszt jelleggel
%N = N + guard_time_sample_number;

bemenet = [];
szamlalo = 1;
for I=k_base(1):k_base(end)
    if k_base(szamlalo) == I
        bemenet = [bemenet modulation_values(s+1,szamlalo)];
        szamlalo = szamlalo + 1;
    else
        bemenet = [bemenet 0];
    end
end

bemenet = [zeros(1,round(fr*Tu) + k_base(1) - 1) bemenet];

ido_tart = real(ifft(bemenet, N)*N);
%megcsinalom a guard time-ot
ido_tart = [zeros(1,guard_time_sample_number) ido_tart];
for I = 1:guard_time_sample_number
    ido_tart(I) = ido_tart(N+I);
end

%parameter = 12000;

%rNormCurFreqOffset = -2 * pi *	(parameter - 6000) / 48000;

% 	cExpStep = exp(j* rNormCurFreqOffset);
% 
% 	cCurExp = 1;
%     
% if cExpStep ~= 1
% 	
% 		for i = 1:size(valamiertek_ido,2)
% 		
% 			valamiertek_ido(i) = valamiertek_ido(i) * conj(cCurExp);
% 			
% %			/* Rotate exp-pointer on step further by complex multiplication
% %			   with precalculated rotation vector cExpStep. This saves us from
% %			   calling sin() and cos() functions all the time (iterative
% %			   calculation of these functions) */
% 			cCurExp = cCurExp * cExpStep;
%         end
%         
% end
      
time_domain2 = [time_domain2 ido_tart];     
    
%_________________________________________________
    
    
%az aktualis szimbolum idejenek szamitasa
	time_index_begin = length(time)+1;

	t_counter = s*Ts;
	while t_counter < (s+1)*Ts,
		time(length(time)+1) = t_counter;
		t_counter = t_counter + T_min;
	end

	time_index_end = length(time);

%az ido tartomanybeli ertekek inicializalasa
	for I = time_index_begin:time_index_end,
		time_domain(I) = 0;
	end

%A konkret szamitasok:

	for I = 1:size(k_base,2),

		t_counter = 0;
		time_index_counter = time_index_begin;

		while time_index_counter <= time_index_end,
			time_domain(time_index_counter) = real( time_domain(time_index_counter) + exp(j*2*pi*fr*t_counter)*modulation_values(s+1,I)*exp(j*2*pi*k_base(I)/Tu*(t_counter-Tg)));

            t_counter = t_counter + T_min;
			time_index_counter = time_index_counter + 1;
		end
        
    end

%_________________________________
% fr = 4500;
% time_domain_temp=[];
% for vivo_szamlalo = 1:size(modulation_values,2)
%     time_domain_temp_one_car=[];
%     for t = 0:t_lepes:Ts
%         time_domain_temp_one_car = [time_domain_temp_one_car abs(modulation_values(s+1,vivo_szamlalo))*cos(2*pi*(fr+f_lepes*k_base(vivo_szamlalo))*(t-Tg)+angle(modulation_values(s+1,vivo_szamlalo)))];
%     end
%     if time_domain_temp
%         time_domain_temp = time_domain_temp + time_domain_temp_one_car;
%     else
%         time_domain_temp = time_domain_temp_one_car;
%     end
% end
% 
% time_domain3 = [time_domain3 time_domain_temp];
% fr=carrier_frequency;
%______________________________________
    
    
    
    
%kovetkezo OFDM szimbolum jon
	s = s + 1;

    disp(sprintf('%d/%d szimbolum',s, size(modulation_values,1)));
    
    
end


%***************************************
%kapott eredmeny abrazolasa

%figure;
%plot(time,time_domain);

%xlabel('Ido [s]');
%ylabel('Amplitudo');

% figure;
% win=hamming(length(time_domain)); %ablakozas, hogy csokkenjen a fr szivargas
% winr = win.';
% 
% a=abs(fft(time_domain.*winr));
% a=a(1:floor(end/2));
% plot((0:length(a)-1)/(T_min*length(time_domain)),a);
% 
% xlabel('Frekvencia [Hz]');
% ylabel('Amplitudo');
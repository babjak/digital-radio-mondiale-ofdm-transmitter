%		DRM 64QAM HMsym UEP encoder for MSC
%
%
%	Babjak Benjamin
%	2005.08.13	
%
%
%
%	Leiras:
%
%A program a DRM rendszer blokk diagramjan definialt "channel encoder"
%egyseg feladatait latja el az MSC csatornan. A beerkezo biteket kodolja, 
%"megkeveri" majd a definialt konstellacios diagramm alapjan komplex erteku
%cellakat szamit.
%
%
%
%	Hasznalat:
%
%A program bemeno parametereket fogad:
%
%data_in:   Az MSC csatorna adatbitjeit tartalmazo vektor.
%
%VSPP_data_in:   Az MSC csatorna erosen vedett adatbitjeit tartalmazo vektor.
%
%protection_level_1:	Az erosen vedett resz (part A) vedelmi szintjet adja meg.
%
%protection_level_2:	A gyengen vedett resz (part B) vedelmi szintjet adja meg.
%
%X:	Az "A" resz byte-jainak szama, ahogy az SDC-ben jelezve van.
%
%robustness_mode:	A DRM rendszerben definialt modok (A,B,C,D) egyiket
%			kell megadni ezzel a parameterrel.
%
%spectrum_occupancy:	A DRM rendszerben definialt spektrum szelessegek
%			(0, 1, 2, 3, 4, 5) egyiket kell megadni ezzel a 
%			parameterrel. A nagyobb ertek nagyobb spektrum
%			szelesseget jelol.
%
%A bemeno parameterek alapjan a program az MSC csatorna adatait kodolja.
%
%
%
%Megjegyzes: A program bemeno parametereinek formajat erosen koti a DRM 
%szabvany, azaz:
%
%
%
%
%
%
%
%Megjegyzes: A program az "ETSI ES 201 980 V2.1.1" szabvany alapjan
%keszult.
%
%


function [data_out] = drm_MSC_64_HMsym_UEP_encoder(data_in, VSPP_data_in, protection_level_1, protection_level_2, X, robustness_mode, spectrum_occupancy)



%***************************************
%bemeno parameterek ellenorzese
data_out = [];

if robustness_mode ~= 'A' & robustness_mode ~= 'B' & robustness_mode ~= 'C' & robustness_mode ~= 'D'
	disp('robustness_mode ertekei A, B, C, D lehet');
	return;
end

if robustness_mode == 'A' robustness_mode = 0; end
if robustness_mode == 'B' robustness_mode = 1; end
if robustness_mode == 'C' robustness_mode = 2; end
if robustness_mode == 'D' robustness_mode = 3; end

if isstr(spectrum_occupancy) 
	disp('spectrum_occupancy ertekei 0, 1, 2, 3, 4, 5, lehet');
	return;
end

spectrum_occupancy = rem(round(abs(spectrum_occupancy)),6);

if ~(spectrum_occupancy == 3 | spectrum_occupancy == 5 | robustness_mode < 2)
	disp('spectrum_occupancy es robustness_mode egyuttesen nem vehet fel ilyen ertekeket');
	return;
end

if isstr(protection_level_1) 
	disp('protection_level_1 ertekei 0, 1, 2, 3 lehet');
	return;
end

protection_level_1 = rem(round(abs(protection_level_1)),4);

if isstr(protection_level_2) 
	disp('protection_level_2 ertekei 0, 1, 2, 3 lehet');
	return;
end

protection_level_2 = rem(round(abs(protection_level_2)),4);

if protection_level_2 <=protection_level_1
	disp('A protection_level_2-nek magasabbnak kell lenni a protection_level_1-nel');
	return;
end
    
%********************************************
%valtozok kezdoertekeinek beallitasa
R_table = [1/2 3/10 3/5 10; 4/7 4/11 8/11 11; 3/5 4/7 7/8 56; 2/3 2/3 8/9 9];
RYlcm_1 = R_table(protection_level_1+1,4);
Rp_1 = R_table(protection_level_1+1,1:3);

RYlcm_2 = R_table(protection_level_2+1,4);
Rp_2 = R_table(protection_level_2+1,1:3);


RXp_RYp_table = [1/4 3/10 1/3 4/11 1/2 4/7 3/5 2/3 8/11 3/4 4/5 7/8 8/9; 1 3 1 4 1 4 3 2 8 3 4 7 8; 4 10 3 11 2 7 5 3 11 4 5 8 9];

QAM_MSC_cell_number_table = [1259 1422 2632 2959 5464 6118; 966 1110 2051 2337 4249 4774; 0 0 0 1844 0 3867; 0 0 0 1226 0 2606];

NMUX = QAM_MSC_cell_number_table(robustness_mode+1, spectrum_occupancy+1);

p_max = 3;

%part A cells
N1 = ceil(8*X/(2*RYlcm_1*(Rp_1(2)+Rp_1(3)))) * RYlcm_1;
if N1 > 20
	N1 = 20;
end
%part B cells
N2 = NMUX - N1;

for I=2:p_max
	RXp_1(I) = RXp_RYp_table(2, contains(Rp_1(I), RXp_RYp_table, 1));
	RYp_1(I) = RXp_RYp_table(3, contains(Rp_1(I), RXp_RYp_table, 1));
end

for I=1:p_max
	RXp_2(I) = RXp_RYp_table(2, contains(Rp_2(I), RXp_RYp_table, 1));
	RYp_2(I) = RXp_RYp_table(3, contains(Rp_2(I), RXp_RYp_table, 1));
end

%L1=0;
%for I=1:p_max
%	L1 = L1 + 2*N1*Rp(I);
%end

%L2=0;
%for I=1:p_max
%	L2 = L2 + RXp*floor((2*N2-12)/RYp);
%end

for I=2:p_max
%jobban vedett bitekre
	M(I,1) = 2*N1*Rp_1(I);
%kevesbe vedett bitekre
	M(I,2) = RXp_2(I)*floor((2*N2-12)/RYp_2(I));
end

M(1,2) = RXp_2(1)*floor((2*(N1+N2)-12)/RYp_2(1));

if size(data_in, 2) ~= (M(2,1) + M(3,1) + M(2,2) + M(3,2)) | size(data_in, 1) ~= 1
   disp(sprintf('data_in-nak egy sorvektornak kell lennie %d elemmel', (M(2,1) + M(3,1) + M(2,2) + M(3,2))));
   return;
end

if size(VSPP_data_in, 2) ~= M(1,2) | size(VSPP_data_in, 1) ~= 1
   disp(sprintf('VSPP_data_in-nak egy sorvektornak kell lennie %d elemmel', M(1,2)));
   return;
end


for I=2:p_max
	rp(I) = (2*N2 -12)-RYp_2(I)*floor((2*N2-12)/RYp_2(I));
end

rp(1) =  (2*(N1+N2) -12)-RYp_2(1)*floor((2*(N1+N2)-12)/RYp_2(1));

a = 1 / sqrt(42);


%********************************************
%szamitasok

%particionalas es kodolas
%erosen vedett
%p=0

%p=1
v1_1 = encoder(data_in(1:M(2,1)), 0, 0, Rp_1(2), 0);  

%p=2
v2_1 = encoder(data_in(M(2,1)+1:M(2,1)+M(3,1)), 0, 0, Rp_1(3), 0); 

%gyengen vedett
%p=1
v1_2 = encoder(data_in(M(2,1)+M(3,1)+1:M(2,1)+M(3,1)+M(2,2)), 0, 1, Rp_2(2), rp(2)); 

%p=2
v2_2 = encoder(data_in(M(2,1)+M(3,1)+M(2,2)+1:end), 0, 1, Rp_2(3), rp(3));

%VSPP
v0_2 =  encoder(VSPP_data_in(1:end), 0, 1, Rp_2(1), rp(1)); 


%interleaving
%p=0
y0 = v0_2;

%p=1
y1 = [interleaving(v1_1, 13) interleaving(v1_2, 13)];

%p=2
y2 = [interleaving(v2_1, 21) interleaving(v2_2, 21)];


while length(y0)
	temp1 = y0(1)*1 + y1(1)*2 + y2(1)*4;
    temp2 = y0(2)*1 + y1(2)*2 + y2(2)*4;

	data_out(end+1) = (7-2*temp1)*a + (7-2*temp2)*a*j;

   	y0 = y0(3:end);
	y1 = y1(3:end);
	y2 = y2(3:end);
end

return;

%Foprogram vege
%********************************************
%Seged fuggvenyek


function data_out = interleaving(data_in, tp)
	new_values = pi_function(length(data_in), tp) + 1;
	for I = 1:length(data_in)
		data_out(I) = data_in(new_values(I));
	end

	return





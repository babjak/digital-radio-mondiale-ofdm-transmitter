+-----------------------+--------------------------------+
| Block                 | Program                        |
+-----------------------+--------------------------------+
| SDC pre-coder         | drm_SDC_pre_coder_CRC_word.m   |
+-----------------------+--------------------------------+
| FAC pre-coder         | drm_FAC_pre_coder_CRC_word.m   |
+-----------------------+--------------------------------+
| Energy dispersal      | drm_energy_dispersal.m         |
+-----------------------+--------------------------------+
| Channel encoder       | drm_FAC_4_SM_EEP_encoder.m     |
|                       | drm_SDC_16_SM_EEP_encoder.m    |
|                       | drm_SDC_4_SM_EEP_encoder.m     |
|                       | drm_MSC_16_SM_EEP_encoder.m    |
|                       | drm_MSC_16_SM_UEP_encoder.m    |
|                       | drm_MSC_64_SM_EEP_encoder.m    |
|                       | drm_MSC_64_SM_UEP_encoder.m    |
|                       | drm_MSC_64_HMsym_EEP_encoder.m |
|                       | drm_MSC_64_HMsym_UEP_encoder.m |
|                       | drm_MSC_64_HMmix_EEP_encoder.m |
|                       | drm_MSC_64_HMmix_UEP_encoder.m |
+-----------------------+--------------------------------+
| Cell interleaver      |                                |
| Pilot generator       | drm_interleaver.m              |
+-----------------------+--------------------------------+
| OFDM cell mapper      | drm_cell_mapper.m              |
+-----------------------+--------------------------------+
| OFDM signal generator | drm_OFDM.m                     |
+-----------------------+--------------------------------+
